package com.example.zuitt.Application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/*Annotations*/
@SpringBootApplication
@RestController
/*This tells Spring Boot that this is an endpoint that will be used in handling web pages*/
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s!", name);
	}

	@GetMapping("/shape")
	public String shape(@RequestParam(value = "shape", defaultValue = "error") String shapeName, int numberOfSides){
		return String.format("Your shape is " + shapeName + ". It has " + numberOfSides + " sides.");
	}

}
